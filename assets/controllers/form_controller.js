import { Controller } from '@hotwired/stimulus';
import Airtable from 'airtable';

export default class extends Controller {
    connect() {
        // this.element is the form
        const drinkForm = this.element;

        // Get data from localStorage
        const currentMGsStorage = localStorage.getItem('totalMGs');
        const currentDrinksStorage = localStorage.getItem('drinks');

        // Start by getting the caffeine options
        const base = new Airtable({apiKey: 'keyHjButoEupzm6xQ'}).base('app7B8WGF9q68sT4H');

        // Set variables for use on the page
        let drinkOptions = [];
        const drinkOptionsDropdown = document.getElementById('drinkOptions');
        const drinkOptionsDescription = document.getElementById('drinkDescription');
        const drinkCount = document.getElementById('drinkCount');
        const formError = document.getElementById('formError');
        const resetVjsDataBtn = document.getElementById('resetVjsData');
        const totalMGNotice = document.getElementById('totalMGNotice');

        // Update current total notice
        totalMGNotice.innerText = currentMGsStorage;

        // Connect with Airtable and get the data
        base('Drinks').select({
            view: 'Grid view'
        }).firstPage(function(err, records) {
            if (err) { console.error(err); return; }
            records.forEach(function(record) {
                const strippedName = record.get('name').toLowerCase().replaceAll(' ', '-');

                // Add to the dropdown
                drinkOptionsDropdown.insertAdjacentHTML('beforeend', `<option value="${strippedName}">${record.get('name')}</option>`);

                // Add to the drinks array
                drinkOptions.push({
                    strippedName,
                    name: record.get('name'),
                    caffeinePerServing: record.get('caffeinePerServing'),
                    servingSize: record.get('servingSize'),
                    description: record.get('description')
                })
            });
        });

        /**
         * Get the current selected drink
         *
         * @returns {*[]}
         */
        function getCurrentDrink() {
            return drinkOptions.filter((drink) => drink.strippedName === drinkOptionsDropdown.value);
        }

        /**
         * Event Handler for updating the drink description
         */
        function updateDescription() {
            drinkOptionsDescription.innerText = '';

            const theDrink = getCurrentDrink();

            if (theDrink.length > 0) {
                drinkOptionsDescription.innerText = theDrink[0].description;
            }
        }

        // Setup event listeners
        drinkOptionsDropdown.addEventListener('change', updateDescription);
        resetVjsDataBtn.addEventListener('click', (e) => {
            localStorage.setItem('drinks', JSON.stringify([]));
            localStorage.setItem('totalMGs', '0');

            window.location = '/vanilla-js-index';
        });
        drinkForm.addEventListener('submit', (e) => {
            e.preventDefault();

            const theDrink = getCurrentDrink();

            if (theDrink.length > 0) {
                // Build obj for the drink saved data
                const drinkData = {
                    quantity: drinkCount.value,
                    drink: theDrink[0]
                }

                // Calculate MGs on this selection
                const thisMGs = (parseInt(theDrink[0].servingSize) * parseInt(drinkCount.value)) * parseInt(theDrink[0].caffeinePerServing.replace('mg', ''));

                // Make sure we are not going over 500MGs
                let totalMGs = thisMGs;

                if (currentMGsStorage) {
                    totalMGs += parseInt(currentMGsStorage);
                }

                if (totalMGs >= 500) {
                    formError.innerText = `Sorry, your total would be ${totalMGs}mg and this entry will put you over the 500mg caffeine limit!`;
                } else {
                    // Start saving the data to localStorage
                    // If first time saving data
                    if (!currentMGsStorage && !currentDrinksStorage) {
                        const drinkArray = [];
                        drinkArray.push(drinkData);
                        localStorage.setItem('drinks', JSON.stringify(drinkArray));
                        localStorage.setItem('totalMGs', thisMGs.toString());

                        // Navigate to Results page
                        window.location = '/vanilla-js-results';
                    } else {
                        const currentDrinks = JSON.parse(currentDrinksStorage);
                        currentDrinks.push(drinkData);

                        localStorage.setItem('totalMGs', totalMGs.toString());
                        localStorage.setItem('drinks', JSON.stringify(currentDrinks));

                        // Navigate to Results page
                        window.location = '/vanilla-js-results';
                    }
                }
            } else {
                formError.innerText = 'Please select a drink';
            }
        });
    }
}
