import { Controller } from '@hotwired/stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        this.resultstable = this.element;
        const resultsTableBody = document.getElementById('resultsTableBody');
        const totalConsumedSpan = document.getElementById('totalConsumed');

        const currentMGsStorage = localStorage.getItem('totalMGs');
        const currentDrinksStorage = JSON.parse(localStorage.getItem('drinks'));

        totalConsumedSpan.innerText = currentMGsStorage;

        currentDrinksStorage.forEach((drink, index) => {
            const totalMGs = (parseInt(drink.quantity) * parseInt(drink.drink.servingSize)) * parseInt(drink.drink.caffeinePerServing.replace('mg', ''));

            resultsTableBody.insertAdjacentHTML('beforeend', `
                <tr>
                    <td>${index + 1}</td>
                    <td>${drink.drink.name}</td>
                    <td>${drink.quantity}</td>
                    <td>${drink.drink.servingSize}</td>
                    <td>${totalMGs}mg</td>
                </tr>
            `)
        })
    }
}
