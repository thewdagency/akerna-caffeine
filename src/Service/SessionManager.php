<?php
// src/Service/SessionManager.php
namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;

class SessionManager
{
	private $session;

	/**
	 * @var
	 */
	private $totalMGs;

	/**
	 * @var
	 */
	private $drinks;

	public function __construct(RequestStack $requestStack) {
		$this->session = $requestStack->getSession();
		$this->totalMGs = $this->session->get('totalMGs');
		$this->drinks = $this->session->get('drinks');
	}

	public function setTotalMGs($val) {
		$this->session->set('totalMGs', $val);
	}

	public function setDrinks($array) {
		$this->session->set('drinks', $array);
	}

	public function getTotalMGs() {
		return $this->totalMGs;
	}

	public function getDrinks() {
		return $this->drinks;
	}
}
