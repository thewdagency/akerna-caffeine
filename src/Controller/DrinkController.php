<?php
// src/Controller/DrinkController.php
namespace App\Controller;

use App\Entity\Drinks;
use App\Service\SessionManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DrinkController extends AbstractController {
	/**
	 * @Route("/php-drinks", name="phpDrinks")
	 *
	 * @param \Doctrine\Persistence\ManagerRegistry $doctrine
	 * @param \App\Service\SessionManager $sessionManager
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function drinkForm(ManagerRegistry $doctrine, SessionManager $sessionManager, Request $request): Response
	{
		$totalMGs = $sessionManager->getTotalMGs();

		$error = '';

		// Check for error param
		if ($request->query->get('error')) {
			$error = $request->query->get('error');
		}

		if (!$totalMGs) {
			$totalMGs = 0;
		}

		$drinkRepository = $doctrine->getRepository(Drinks::class);
		$allDrinks = $drinkRepository->findAll();

		return $this->render('phpIndex.html.twig', [
			'error' => $error,
			'drinks' => $allDrinks,
			'totalMGs' => $totalMGs
		]);
	}

	/**
	 * @Route("/php-drinks/form-handler")
	 *
	 * @param \Doctrine\Persistence\ManagerRegistry $doctrine
	 * @param \App\Service\SessionManager $sessionManager
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function handleForm(ManagerRegistry $doctrine, SessionManager $sessionManager, Request $request): Response
	{
		if ($request->get('drinkOptions') && $request->get('drinkCount')) {
			$drinkRepository = $doctrine->getRepository(Drinks::class);
			$allDrinks = $drinkRepository->findAll();

			foreach ($allDrinks as $drink) {
				$unStrippedName = ucwords(str_replace('-', ' ', $request->get('drinkOptions')));
				// Check if "Nos" exists because this should be all uppercase
				$unStrippedName = str_replace('Nos', 'NOS', $unStrippedName);

				if ($unStrippedName === $drink->getName()) {
					// Build array of this drink
					$thisDrinkArray = [
						'name' => $drink->getName(),
						'caffeineServingSize' => $drink->getCaffeinePerServing(),
						'servingSize' => $drink->getServingSize(),
						'quantity' => $request->get('drinkCount')
					];

					// Calculate the total MGs based upon selection
					$thisDrinkCaffeine = (int) str_replace('mg', '', $thisDrinkArray['caffeineServingSize']);
					$totalCaffeineThisSelection = ($thisDrinkCaffeine * (int) $thisDrinkArray['servingSize']) * (int) $request->get('drinkCount');

					// Add to the drink array
					$thisDrinkArray['totalDrinkMGs'] = $totalCaffeineThisSelection;

					// Update the session for totalMGs
					$currentSessionTotalMGs = (int) $sessionManager->getTotalMGs();
					$newTotalMGs = $currentSessionTotalMGs + $totalCaffeineThisSelection;

					// See if total is more than limit: 500mg
					if ($newTotalMGs > 500) {
						$error = 'Your new total would be ' .$newTotalMGs. 'mg and this would bring you over 500mg!';

						return $this->redirectToRoute('phpDrinks', ['error' => $error]);
					} else {
						$sessionManager->setTotalMGs($newTotalMGs);

						// Update the session values for the results list
						$currentSessionSelections = $sessionManager->getDrinks();

						if (!$currentSessionSelections) {
							$buildDrinksSessionArray   = [];
							$buildDrinksSessionArray[] = $thisDrinkArray;

							$sessionManager->setDrinks($buildDrinksSessionArray);
						} else {
							$currentSessionSelections[] = $thisDrinkArray;
							$sessionManager->setDrinks($currentSessionSelections);
						}
					}
				}
			}
		} else {
			$error = 'Please select a drink';

			return $this->redirectToRoute('phpDrinks', ['error' => $error]);
		}

		return $this->redirectToRoute('phpResults');
	}

	/**
	 * @Route("/php-drinks/clear-drinks")
	 *
	 * @param \App\Service\SessionManager $sessionManager
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function resetSession(SessionManager $sessionManager): Response
	{
		$sessionManager->setTotalMGs(null);
		$sessionManager->setDrinks(null);

		return $this->redirectToRoute('phpDrinks');
	}

	/**
	 * @Route("/php-results", name="phpResults")
	 *
	 * @param \Doctrine\Persistence\ManagerRegistry $doctrine
	 * @param \App\Service\SessionManager $sessionmanager
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function resultsForm(ManagerRegistry $doctrine, SessionManager $sessionmanager): Response
	{
		$totalMGs = $sessionmanager->getTotalMGs();

		if (!$totalMGs) {
			$totalMGs = 0;
		}

		$currentDrinks = $sessionmanager->getDrinks();

		if (!$currentDrinks) {
			$currentDrinks = [];
		}

		return $this->render('phpResults.html.twig', [
			'drinks' => $currentDrinks,
			'totalMGs' => $totalMGs
		]);
	}

	/**
	 * @Route("/php-drinks/add-default-drinks")
	 *
	 * @param \Doctrine\Persistence\ManagerRegistry $doctrine
	 * @param \Symfony\Component\Validator\Validator\ValidatorInterface $validator
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function addDefaultDrinks(ManagerRegistry $doctrine, ValidatorInterface $validator): Response
	{
		$defaultDrinks = [
			[
				'name' => 'Monster Ultra Sunrise',
				'strippedName' => 'monster-ultra-sunrise',
				'caffeinePerServing' => '75mg',
				'servingSize' => '2',
				'description' => 'A refreshing orange beverage that has 75mg of caffeine per serving. Every can has two servings.',
			],
			[
				'name' => 'Black Coffee',
				'strippedName' => 'black-coffee',
				'caffeinePerServing' => '95mg',
				'servingSize' => '1',
				'description' => 'The classic, the average 8oz. serving of black coffee has 95mg of caffeine. ',
			],
			[
				'name' => 'Americano',
				'strippedName' => 'americano',
				'caffeinePerServing' => '77mg',
				'servingSize' => '1',
				'description' => 'Sometimes you need to water it down a bit... and in comes the americano with an average of 77mg. of caffeine per serving.',
			],
			[
				'name' => 'Sugar Free NOS',
				'strippedName' => 'sugar-free-nos',
				'caffeinePerServing' => '130mg',
				'servingSize' => '2',
				'description' => 'Another orange delight without the sugar. It has 130 mg. per serving and each can has two servings.',
			],
			[
				'name' => '5 Hour Energy',
				'strippedName' => '5-hour-energy',
				'caffeinePerServing' => '200mg',
				'servingSize' => '1',
				'description' => 'An amazing shot of get up and go! Each 2 fl. oz. container has 200mg of caffeine to get you going.',
			]
		];

		// Setup Entity Manager
		$entityManager = $doctrine->getManager();

		// Prep repo to check for existing items
		$drinkRepository = $doctrine->getRepository(Drinks::class);

		foreach ($defaultDrinks as $defaultDrink) {
			// See if this drink already exists
			$thisDrink = $drinkRepository->findOneBy(['name' => $defaultDrink['name']]);
			// Get count of all items persisted
			$persistedDrinks = 0;

			if (!$thisDrink) {
				// Add the new drink
				$drink = new Drinks();
				$drink->setName($defaultDrink['name']);
				$drink->setCaffeinePerServing($defaultDrink['caffeinePerServing']);
				$drink->setServingSize($defaultDrink['servingSize']);
				$drink->setDescription($defaultDrink['description']);

				// Persist this drink
				$entityManager->persist($drink);

				// Check for errors
				$errors = $validator->validate($drink);
				if (count($errors) > 0) {
					return new Response((string) $errors, 400);
				}

				$persistedDrinks += 1;
			}
		}

		// Check if any drinks were persisted, and if so, add them to DB
		if ($persistedDrinks > 0) {
			$entityManager->flush();

			return $this->render('newDrinksAdded.html.twig', [
				'message' => 'All drinks saved successfully'
			]);
		} else {
			return $this->render('newDrinksAdded.html.twig', [
				'message' => 'No new drinks added, was this already done?'
			]);
		}
	}
}
