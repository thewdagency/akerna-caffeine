<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
	/**
	 * @Route("/")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
	public function index(): Response
	{
		return $this->render('index.html.twig');
	}

	/**
	 * @Route("/vanilla-js-index")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
	public function vanillaJsIndex(): Response
	{
		return $this->render('vanillaJsIndex.html.twig');
	}

	/**
	 * @Route("/vanilla-js-results")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
	public function vanillaJsResults(): Response
	{
		return $this->render('vanillaJsResults.html.twig');
	}
}
