<?php

namespace App\Entity;

use App\Repository\DrinksRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DrinksRepository::class)
 */
class Drinks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $caffeinePerServing;

    /**
     * @ORM\Column(type="integer")
     */
    private $servingSize;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCaffeinePerServing(): ?string
    {
        return $this->caffeinePerServing;
    }

    public function setCaffeinePerServing(string $caffeinePerServing): self
    {
        $this->caffeinePerServing = $caffeinePerServing;

        return $this;
    }

    public function getServingSize(): ?int
    {
        return $this->servingSize;
    }

    public function setServingSize(int $servingSize): self
    {
        $this->servingSize = $servingSize;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
